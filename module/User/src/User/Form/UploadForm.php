<?php

namespace User\Form;

/**
 * Description of UploadForm
 *
 * @author RD
 */
use Zend\Form\Form;

class UploadForm extends Form {

    public function __construct($name = null) {
        parent::__construct('upload');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'file',
            'type' => 'File',
            'options' => array(
                'label' => 'File',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Feltölt',
                'id' => 'upload_btn',
            ),
        ));
    }

}

?>
