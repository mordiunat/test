<?php

namespace User\Model;

/**
 * FelhasználóTáblát reprezentáló <i>UserTable</i> osztály.
 *
 * @author RD
 */
use Zend\Db\TableGateway\TableGateway;

class UserTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * A tábla teljes tartalma
     * @return ResultSet
     */
    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Adatbázisból ID szerint lekéri a felhasználót.
     * @param int $id
     * @return <i>User</i>
     * @throws \Exception 
     */
    public function getUser($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Az adatbázisból felhasználónév és jelszó alapján lekéri a felhasználót.
     * @param string $username
     * @param string $password
     * @return <i>User</i>
     */
    public function getLoginUser($username, $password) {
        $pass = md5($password);
        $rowset = $this->tableGateway->select(array('username' => $username, 'password' => $pass));
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * Elmenti a felhasználót az adatbázisba.
     * @param \User\Model\User $user
     * @throws \Exception
     */
    public function saveUser(User $user) {
        $data = array(
            'name' => $user->name,
            'email' => $user->email,
            'username' => $user->username,
            'password' => $user->password,
            'role' => $user->role,
        );

        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    /**
     * Törli az adatbázisból a felhasználót.
     * @param type $id
     */
    public function deleteUser($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}

?>
